import React from 'react'
import {configure, shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { App } from './components/App/App'
import { CharactersContainer } from "./components/Characters";

configure({
  adapter: new Adapter()
})

describe('<App />', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<App />)
  })

  it('Should render', () => {
    expect(wrapper.find(CharactersContainer))
  })

  it('Should render', () => {
    expect(wrapper.find(CharactersContainer))
  })
})