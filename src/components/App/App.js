import {useFetchFromAPI} from "../../hooks/useFetch";
import {CharactersContainer} from "../Characters";
import {CharacterCard} from "../Characters/Card";
import {
    Button,
    ButtonGroup,
    Container,
    LinearProgress
} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";
import ArrowBackIosIcon
    from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon
    from '@material-ui/icons/ArrowForwardIos';

const useStyles = makeStyles({
    root: {
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "center"
    },
    btn: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
});

export const App = () => {
    const classes = useStyles();
    const [
        state,
        loading,
        setUrl,
        nextPage,
        prevPage
    ] = useFetchFromAPI();

    const nextPageHandler = () => {
        setUrl(nextPage)
    }
    const prevPageHandler = () => {
        setUrl(prevPage)
    }
    return (
        <CharactersContainer
            className={classes.root}
            loading={loading}>
            <h1>Hello bro!</h1>
            <Container>
                {loading ?
                    <LinearProgress/> :
                    <>
                        {
                            state.map((data, index) => (
                                <CharacterCard
                                    key={index}
                                    loading={loading}
                                    character={data}/>
                            ))
                        }
                        <ButtonGroup>
                            {prevPage !== null ?
                                <Button
                                    className={classes.btn}
                                    onClick={prevPageHandler}>
                                    <ArrowBackIosIcon/>
                                </Button>
                                : ""}
                            {nextPage !== null ?
                                <Button
                                    className={classes.btn}
                                    onClick={nextPageHandler}>
                                    <ArrowForwardIosIcon/>
                                </Button>
                                : ""}

                        </ButtonGroup>
                    </>
                }
            </Container>
        </CharactersContainer>
    )
}

