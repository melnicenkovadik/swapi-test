import React, {useState} from 'react';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup';
import {
    Button,
    CircularProgress,
    Container
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    form: {
        width: 320
    },
    input: {
        minHeight: 30,
        border: "1px solid #ccc",
    },
    btn: {
        display: 'flex',
        flexDirection: 'column',
        width: "auto",
        margin: 0,
        padding: 10,
        backgroundColor: '#eaeaea'
    },
}));
const FormInput = (props) => {
    const [loading, setLoading] = useState(false);
    const classes = useStyles()
    const formikProps = {
        initialValues: {comment: '', color: ''},
        validationSchema: Yup.object({
            comment: Yup
                .string()
                .required('Sorry, input is required')
                .max(100, `Sorry, the comment to ${props.name} is too long`)
                .min(3, `Sorry, the comment to ${props.name} mast have 3 characters`)

        }),
        onSubmit: values => {
            values.comment = ''
            setLoading(true)
            const proxyUrl = 'https://cors-anywhere.herokuapp.com/';

            fetch(proxyUrl + props.url, {
                method: 'POST',
                headers: {'Content-Type': 'application/json',},
                body: JSON.stringify(values.comment)
            })
                .then(function (response) {
                    if (response.status === 200) {
                        props.setTextAlert(`Comment to ${props.name} was added`)
                    } else {
                        props.setTextAlert('Error')
                    }
                    props.setOpen(true);
                    setLoading(false)
                    return response.json();
                });
        }
    }
    return (
        <Formik
            {...formikProps}>
            {formik => (
                <Container className={classes.form}>
                    <Form>
                        {
                            !loading ?
                                <>
                                    <Field
                                        className={classes.input}
                                        placeholder={props.placeholder}
                                        name="comment"
                                        type="text"/>
                                    <br/>
                                    <ErrorMessage
                                        name="comment"/>
                                </> : <CircularProgress/>

                        }

                        <Button
                            disabled={!formik.values.comment.trim()}
                            className={classes.btn}
                            type="submit">
                            Comment
                        </Button>
                    </Form>
                </Container>
            )}
        </Formik>
    )
}
export default FormInput;