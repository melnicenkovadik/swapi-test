import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Button} from "@material-ui/core";
import FormInput from "./Input";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: "center",
        justifyContent: "center",
        width: "auto",
        margin: 0,
        padding: 10,
    },

}));

export const AddComment = ({url,name}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [textAlert, setTextAlert] = React.useState('');

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    return (
        <div className={classes.root}>
            {
                open ?
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        open={open}
                        autoHideDuration={6000}
                        onClose={handleClose}
                        setOpen={setOpen}
                        message="Notification"
                        action={
                            <React.Fragment>
                                <Button color="secondary" size="small" onClick={handleClose}>
                                    {textAlert}
                                </Button>
                                <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                                    <CloseIcon fontSize="small"/>
                                </IconButton>
                            </React.Fragment>
                        }
                    /> :
                    null
            }
            <FormInput
                className={classes.root}
                setTextAlert={setTextAlert}
                name={name}
                setOpen={setOpen}
                url={url}
                placeholder={"Your comment..."}/>
        </div>
    );
};
