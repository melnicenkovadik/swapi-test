import React from "react";
import {
    createMuiTheme,
    MuiThemeProvider
} from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {AddComment} from "../AddComment";

export const CharacterCard = ({character}) => {
    const theme = {
        overrides: {
            MuiCard: {
                root: {
                    "&.MuiEngagementCard--01": {
                        transition: "0.3s",
                        width: "auto",
                        margin: "10px",
                        padding: 20,
                        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
                        "&:hover": {
                            boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
                        },
                        "& .MuiCardMedia-root": {
                            paddingTop: "56.25%",

                        },
                        "& .MuiCardContent-root": {
                            textAlign: "left",
                            padding: 15,
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                        },
                        "& .MuiTypography--heading": {
                            fontWeight: "bold"
                        },
                        "& .MuiTypography--subheading": {
                            lineHeight: 1.8
                        },
                    }
                }
            }
        }
    };
    return (
        <MuiThemeProvider
            theme={createMuiTheme(theme)}>
            <div
                className="App">
                <Card
                    className={"MuiEngagementCard--01"}>
                    <CardContent
                        className={"MuiCardContent-root"}>
                        <div>
                            <Typography
                                className={"MuiTypography--heading"}
                                variant={"h5"}
                                gutterBottom>
                                {character.name}
                            </Typography>
                            {character.birth_year.trim() !== 'unknown' ?
                                <Typography
                                    variant={"h6"}
                                    className={"MuiTypography--subheading"}>
                                    Birth year:{character.birth_year}
                                </Typography>
                                : ""
                            }

                        </div>
                        <div>
                            <AddComment
                                name={character.name}
                                url={character.url}/>
                        </div>
                    </CardContent>
                </Card>
            </div>
        </MuiThemeProvider>
    );
}
