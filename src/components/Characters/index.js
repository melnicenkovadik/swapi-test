import React from "react";
import {makeStyles} from "@material-ui/styles";
import {Container} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    root: {
        minHeight: 500,
        minWidth: 500,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: "center",
    },
}));
export const CharactersContainer = (props) => {
    const classes = useStyles()
    return (
        <>
            {
                <Container
                    className={classes.root}>
                    {props.children}
                </Container>
            }

        </>

    );
};