import {useEffect, useState} from "react";

export const useFetchFromAPI = () => {
    const [state, setState] = useState([]);
    const [prevPage, setPrevPage] = useState();
    const [nextPage, setNextPage] = useState();
    const [loading, setLoading] = useState(false);
    const [url, setUrl] = useState(`https://swapi.dev/api/people/`);
    const getComments = async () => {
        setLoading(true);
        const posts = await (await fetch(url)).json();
        setState(posts.results);
        setPrevPage(posts.previous);
        setNextPage(posts.next);
        setLoading(false);
    };
    useEffect(() => {
        try {
            getComments();
        } catch (e) {
            throw e;
        }
    }, [url]);

    return [state, loading, setUrl, nextPage, prevPage];
};
